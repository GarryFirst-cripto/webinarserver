import jwt from 'jsonwebtoken';
import { env } from '../../config/dataConfig';
import { ITokenData } from '../interfaces/interfaces';

const { crypto: { secretKey: key, secretExp: period } } = env;

export const createAccessToken = (data: ITokenData) => jwt.sign(data, key, { expiresIn: period });
export const createRefreshToken = (data: ITokenData) => {
  const { id, mode: reff } = data;
  return jwt.sign({ id, reff }, key);
};

export const fetchTokenData = (token: string):ITokenData => {
  if (token) {
    try {
      return <ITokenData> jwt.verify(token, key);
    } catch (e) {
      return null;
    }
  }
  return null;
};
