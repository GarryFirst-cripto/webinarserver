
const asyncForEach = async (fn: { (item: string): Promise<void>; (arg0: any): any; }, list: string | any[]) => {
  for (let i = 0; i < list.length; i++) {
    await fn(list[i]);
  }
};

export default asyncForEach;
