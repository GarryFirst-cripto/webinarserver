export let host = '';
export const setHost = (value: string) => {
  host = value.includes('localhost') ? `http://${value}/image/` : `https://${value}/image/`;
};