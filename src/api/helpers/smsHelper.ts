import Client from 'twilio';
import { env } from '../../config/dataConfig';

const client = Client(env.smss.accountSid, env.smss.authToken);

const sendSMS = async (phone: string, code: string) => {
  try {
    const send = new Promise((resolve, reject) => {
      client.messages.create({ body: `Your confirm code : ${code}`, from: '+15033609376', to: phone })
      .then((message: any) => { resolve(message.sid)})
      .catch((err: any) => reject(err))
    })
    return await send;
  } catch (e) {
    return { error: 'Twilio auth error ...'}
  }
}

export default sendSMS;
