import { Request, Response } from 'express';
import subsRepository from '../../repositories/subscriptRepository';
import coursesRepository from '../../repositories/coursesRepository';
import { IRequest, IListFilter } from '../interfaces/interfaces';
import { env } from '../../config/dataConfig';

export const getSubsByUserId = async (req: Request, res: Response) => {
  const { user: { mode, id: userId } } = <IRequest> req;
  const { adminMode } = env.admin;
  const id = req.params.id || userId;
  if (mode === adminMode || id === userId) {
    const list = await subsRepository.getByUser(id);
    return res.status(200).send(list);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const getSubsByCourseId = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  const { adminMode } = env.admin;
  const courseId = req.params.id;
  if (mode === adminMode) {
    const list = await subsRepository.getByCourse(courseId);
    return res.status(200).send(list);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const getSubsById = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const id = req.params.id;
    const result = await subsRepository.getById(id);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const getSubsList = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  const { adminMode } = env.admin;
  if (mode === adminMode) {
    const filter = <IListFilter> <unknown>req.query;
    const list = await subsRepository.getList(filter);
    return res.status(200).send(list);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const createSub = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const { courseId } = req.body;
    const course = coursesRepository.getById(courseId);
    if (course) {

      const result = await subsRepository.create(req.body);
      return res.status(200).send(result);      
    }
    return res.status(406).send({ status: 406, message: 'Unknown error ...' });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const updateSub = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  const { adminMode } = env.admin;
  if (mode === adminMode) {
    const { id } = req.body;
    const result = await subsRepository.updateById(id, req.body);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const deleteSub = async (req: Request, res: Response) =>  {
  const { user: { mode } } = <IRequest> req;
  const { adminMode } = env.admin;
  if (mode === adminMode) {
    const id = req.params.id;
    const { result } = await subsRepository.delete(id);
    return res.status(200).send({ result });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}
