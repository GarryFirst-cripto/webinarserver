import { Request, Response } from 'express';
import coursesItemRepository from '../../repositories/coursesItemRepository';
import { IRequest, IListFilter } from '../interfaces/interfaces';
import { host, setHost } from '../helpers/addressHelper';
// import { env } from '../../config/dataConfig';
import * as imageService from './imageServices';

export const getCourseItemsList = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const filter = <IListFilter> <unknown>req.query;
    const list = await coursesItemRepository.getList(filter);
    return res.status(200).send(list);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const getCourseItemById = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const id = req.params.id;
    const result = await coursesItemRepository.getById(id);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const createCourseItem = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const result = await coursesItemRepository.create(req.body);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const updateCourseItem = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const { id } = req.body;
    const result = await coursesItemRepository.updateById(id, req.body);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const deleteCourseItem = async (req: Request, res: Response) =>  {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const id = req.params.id;
    const item = await coursesItemRepository.getById(id);
    let image: boolean = false;
    if (item) {
      const { link } = item;
      image = await imageService.deleteImage(link);
    }
    const { result } = await coursesItemRepository.delete(id);
    return res.status(200).send({ result, image });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const deleteCourseItemLocal = async (id: string) =>  {
  const item = await coursesItemRepository.getById(id);
  let image: boolean = false;
  if (item) {
    const { link } = item;
    image = await imageService.deleteImage(link);
  }
  const { result } = await coursesItemRepository.delete(id);
  return { result, image };
}

export const postImage = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    setHost(req.headers.host);
    const { file: { originalname, buffer } } = req;
    const { params: { id } } = req;
    const item = await coursesItemRepository.getById(id);
    if (item) {
      if (item.link) await imageService.deleteImage(item.link);
      const { fileId } = await imageService.uploadFile(buffer, originalname || 'image.jpg');
      if (fileId) {
        const link = `${host}${fileId}`;
        const courseItem = await coursesItemRepository.updateById(id, { link });
        return res.status(200).send({ link, courseItem });
      }
      return res.status(500).send({ status: 500, message: 'Error writing new image ...' });
    }
    return res.status(500).send({ status: 500, message: 'No such course ...' });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};
