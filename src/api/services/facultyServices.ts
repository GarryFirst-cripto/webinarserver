import { Request, Response } from 'express';
import facultyRepository from '../../repositories/facultyRepository';
import { env } from '../../config/dataConfig';
import { IRequest, IListFilter } from '../interfaces/interfaces';

export const getFacultiesList = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const filter = <IListFilter> <unknown>req.query;
    const list = await facultyRepository.getList(filter);
    return res.status(200).send(list);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const getFacultiesFullList = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const filter = <IListFilter> <unknown>req.query;
    const list = await facultyRepository.getFullList(filter);
    return res.status(200).send(list);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const getFacultyById = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const id = req.params.id;
    const result = await facultyRepository.getById(id);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const createFaculty = async (req: Request, res: Response) => {
  const { adminMode } = env.admin;
  if ((<IRequest>req).user.mode === adminMode) {
    const result = await facultyRepository.create(req.body);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const updateFaculty = async (req: Request, res: Response) => {
  const { adminMode } = env.admin;
  if ((<IRequest>req).user.mode === adminMode) {
    const { id } = req.body;
    const result = await facultyRepository.updateById(id, req.body);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const deleteFaculty = async (req: Request, res: Response) =>  {
    const { adminMode } = env.admin;
  if ((<IRequest>req).user.mode === adminMode) {
    const id = req.params.id;
    const { result } = await facultyRepository.delete(id);
    return res.status(200).send({ result });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}
