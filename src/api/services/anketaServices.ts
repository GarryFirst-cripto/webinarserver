import { Request, Response } from 'express';
import anketasRepository from '../../repositories/anketasRepository';
import { IRequest, IListFilter } from '../interfaces/interfaces';
// import { env } from '../../config/dataConfig';

export const getAnketaList = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const filter = <IListFilter> <unknown>req.query;
    const list = await anketasRepository.getList(filter);
    return res.status(200).send(list);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const getAnketaById = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const id = req.params.id;
    const result = await anketasRepository.getById(id);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const createAnketa = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const result = await anketasRepository.create(req.body);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const updateAnketa = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const { id } = req.body;
    const result = await anketasRepository.updateById(id, req.body);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const deleteAnketa = async (req: Request, res: Response) =>  {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const id = req.params.id;
    const { result } = await anketasRepository.delete(id);
    return res.status(200).send({ result });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}
