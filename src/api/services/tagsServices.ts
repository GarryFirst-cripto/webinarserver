import { Request, Response } from 'express';
import tagsRepository from '../../repositories/tagsrepository';
import { IRequest, IListFilter } from '../interfaces/interfaces';
// import { env } from '../../config/dataConfig';

export const getTagById = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const id = req.params.id;
    const result = await tagsRepository.getById(id);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const getTagsList = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const filter = <IListFilter> <unknown>req.query;
    const list = await tagsRepository.getList(filter);
    return res.status(200).send(list);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const createTag = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const result = await tagsRepository.create(req.body);
    return res.status(200).send(result);      
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const updateTag = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const { id } = req.body;
    const result = await tagsRepository.updateById(id, req.body);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const deleteTag = async (req: Request, res: Response) =>  {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const id = req.params.id;
    const { result } = await tagsRepository.delete(id);
    return res.status(200).send({ result });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}
