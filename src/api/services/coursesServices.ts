import { Request, Response } from 'express';
import coursesRepository from '../../repositories/coursesRepository';
import courseFacultyRepository from '../../repositories/courseFacultyRepository';
import coursesItemRepository from '../../repositories/coursesItemRepository';
import authorsRepository from '../../repositories/authorsRepository';
import reviewsRepository from '../../repositories/reviewsRepository';
import { deleteCourseItemLocal } from './coursesItemServices';
import { deleteScheduleItemLocal } from './scheduleServices';
import schedulesRepository from '../../repositories/schedulesRepository';
import { IRequest, IListFilter } from '../interfaces/interfaces';
import { host, setHost } from '../helpers/addressHelper';
// import { env } from '../../config/dataConfig';
import * as imageService from './imageServices';

export const getCoursesList = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const filter = <IListFilter> <unknown>req.query;
    const list = await coursesRepository.getList(filter);
    list.forEach((item: any) => {
      let mess = '';
      item.faculties.forEach((faculty: any) => {
        if (mess === '') {
          mess = faculty.faculty
        } else {
          mess += ', ' + faculty.faculty
        }
      })
      item.dataValues.faculties = mess;
    });
    return res.status(200).send(list);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const getCoursesFullList = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const filter = <IListFilter> <unknown>req.query;
    const list = await coursesRepository.getFullList(filter);
    return res.status(200).send(list);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const getCourseById = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const id = req.params.id;
    const result = await coursesRepository.getCourseById(id);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const createCourse = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const { id } = await coursesRepository.create(req.body);
    if (id) {
      const { courseitems, schedules, faculties, authors, reviews } = req.body;
      if (courseitems) await coursesItemRepository.setList(id, courseitems);
      if (schedules) await schedulesRepository.setList(id, schedules);
      if (faculties) await courseFacultyRepository.setList(id, faculties);
      if (authors) await authorsRepository.setList(id, authors);
      if (reviews) await reviewsRepository.setList(id, reviews);
    }
    const result = await coursesRepository.getCourseById(id);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const updateCourse = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const { id } = req.body;
    if (id) {
      const { courseitems, schedules, faculties, authors, reviews } = req.body;
      if (courseitems) await coursesItemRepository.setList(id, courseitems);
      if (schedules) await schedulesRepository.setList(id, schedules);
      if (faculties) await courseFacultyRepository.setList(id, faculties);
      if (authors) await authorsRepository.setList(id, authors);
      if (reviews) await reviewsRepository.setList(id, reviews);
      await coursesRepository.updateById(id, req.body);
      const result = await coursesRepository.getCourseById(id);
      return res.status(200).send(result);      
    }
    return res.status(404).send({ status: 404, message: 'Invalid id or id absent ...' });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const joinCourseFaculty = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const { courseId, facultyId } = req.body;
    const link = await courseFacultyRepository.getLink(courseId, facultyId);
    if (!link) await courseFacultyRepository.create({ courseId, facultyId });
    const result = await coursesRepository.getCourseById(courseId);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const exitCourseFaculty = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const { courseId, facultyId } = req.body;
    await courseFacultyRepository.deleteLink(courseId, facultyId);
    const result = await coursesRepository.getCourseById(courseId);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const deleteCourse = async (req: Request, res: Response) =>  {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const id = req.params.id;
    const course = await coursesRepository.getCourseById(id);
    if (course) {
      const { courseitems, schedules } = course;
      const items = courseitems.length;
      courseitems.forEach(async (item: any)=> {
        await deleteCourseItemLocal(item.id);
      });
      const schedulesItems = schedules.length;
      schedules.forEach(async (item: any)=> {
        await deleteScheduleItemLocal(item.id);
      });
      const { result: links } = await courseFacultyRepository.deleteCourse(id);
      const { result: authors } = await authorsRepository.deleteCourse(id);
      const { result: reviews } = await reviewsRepository.deleteCourse(id);
      await imageService.deleteImage(course.link);
      const { result } = await coursesRepository.delete(id);
      return res.status(200).send({ result, items, schedulesItems, links, authors, reviews });
    }
    return { result: 0 };
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const postImage = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    setHost(req.headers.host);
    const { file: { originalname, buffer } } = req;
    const { params: { id } } = req;
    const course = await coursesRepository.getCourseById(id);
    if (course) {
      if (course.link) await imageService.deleteImage(course.link);
      const { fileId } = await imageService.uploadFile(buffer, originalname || 'image.jpg');
      if (fileId) {
        const link = `${host}${fileId}`;
        const newCourse = await coursesRepository.updateById(id, { link });
        return res.status(200).send({ link, newCourse });
      }
      return res.status(500).send({ status: 500, message: 'Error writing new image ...' });
    }
    return res.status(500).send({ status: 500, message: 'No such course ...' });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};

export const getCoursesByTags = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const { params: { id } } = req;
    const filter = <IListFilter> <unknown>req.query;
    const result = await coursesRepository.getByTags(id, filter);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};

export const getCoursesByText = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const { params: { id: text } } = req;
    const filter = <IListFilter> <unknown>req.query;
    const result = await coursesRepository.getByText(text, filter);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};

export const getCoursesByFaculty = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const { params: { id } } = req;
    const filter = <IListFilter> <unknown>req.query;
    const result = await coursesRepository.getByFaculty(id, filter);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};
