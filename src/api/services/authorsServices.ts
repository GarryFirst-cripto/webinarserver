import { Request, Response } from 'express';
import authorsRepository from '../../repositories/authorsRepository';
import { IRequest, IListFilter } from '../interfaces/interfaces';
import { env } from '../../config/dataConfig';
import { host, setHost } from '../helpers/addressHelper';
import * as imageService from './imageServices';

export const getAuthorsList = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const filter = <IListFilter> <unknown>req.query;
    const list = await authorsRepository.getList(filter);
    return res.status(200).send(list);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const getAuthorById = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const id = req.params.id;
    const result = await authorsRepository.getById(id);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const createAuthor = async (req: Request, res: Response) => {
  const { adminMode } = env.admin;
  if ((<IRequest>req).user.mode === adminMode) {
    const result = await authorsRepository.create(req.body);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const updateAuthor = async (req: Request, res: Response) => {
  const { adminMode } = env.admin;
  if ((<IRequest>req).user.mode === adminMode) {
    const { id } = req.body;
    const result = await authorsRepository.updateById(id, req.body);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const deleteAuthor = async (req: Request, res: Response) =>  {
  const { adminMode } = env.admin;
  if ((<IRequest>req).user.mode === adminMode) {
    const id = req.params.id;
    const item = await authorsRepository.getById(id);
    let image: boolean = false;
    if (item) {
      const { link } = item;
      image = await imageService.deleteImage(link);
    }
    const { result } = await authorsRepository.delete(id);
    return res.status(200).send({ result, image });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const deleteAuthorLocal = async (id: string) =>  {
  const item = await authorsRepository.getById(id);
  let image: boolean = false;
  if (item) {
    const { link } = item;
    image = await imageService.deleteImage(link);
  }
  const { result } = await authorsRepository.delete(id);
  return { result, image };
}

export const postImage = async (req: Request, res: Response) => {
  const { adminMode } = env.admin;
  if ((<IRequest>req).user.mode === adminMode) {
    setHost(req.headers.host);
    const { file: { originalname, buffer } } = req;
    const { params: { id } } = req;
    const item = await authorsRepository.getById(id);
    if (item) {
      if (item.link) await imageService.deleteImage(item.link);
      const { fileId } = await imageService.uploadFile(buffer, originalname || 'image.jpg');
      if (fileId) {
        const link = `${host}${fileId}`;
        const newItem = await authorsRepository.updateById(id, { link });
        return res.status(200).send({ link, newItem });
      }
      return res.status(500).send({ status: 500, message: 'Error writing new image ...' });
    }
    return res.status(500).send({ status: 500, message: 'No such course ...' });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};
