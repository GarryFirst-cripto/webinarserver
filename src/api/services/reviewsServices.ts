import { Request, Response } from 'express';
import reviewsRepository from '../../repositories/reviewsRepository';
import { IRequest, IListFilter } from '../interfaces/interfaces';
// import { env } from '../../config/dataConfig';
import { host, setHost } from '../helpers/addressHelper';
import * as imageService from './imageServices';

export const getReviewsList = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const filter = <IListFilter> <unknown>req.query;
    const list = await reviewsRepository.getList(filter);
    return res.status(200).send(list);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const getReviewById = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const id = req.params.id;
    const result = await reviewsRepository.getById(id);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const createReview = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const result = await reviewsRepository.create(req.body);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const updateReview = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const { id } = req.body;
    const result = await reviewsRepository.updateById(id, req.body);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const deleteReview = async (req: Request, res: Response) =>  {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const id = req.params.id;
    const item = await reviewsRepository.getById(id);
    let image: boolean = false;
    if (item) {
      const { link } = item;
      image = await imageService.deleteImage(link);
    }
    const { result } = await reviewsRepository.delete(id);
    return res.status(200).send({ result, image });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const deleteReviewLocal = async (id: string) =>  {
  const item = await reviewsRepository.getById(id);
  let image: boolean = false;
  if (item) {
    const { link } = item;
    image = await imageService.deleteImage(link);
  }
  const { result } = await reviewsRepository.delete(id);
  return { result, image };
}

export const postImage = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    setHost(req.headers.host);
    const { file: { originalname, buffer } } = req;
    const { params: { id } } = req;
    const item = await reviewsRepository.getById(id);
    if (item) {
      if (item.link) await imageService.deleteImage(item.link);
      const { fileId } = await imageService.uploadFile(buffer, originalname || 'image.jpg');
      if (fileId) {
        const link = `${host}${fileId}`;
        const newItem = await reviewsRepository.updateById(id, { link });
        return res.status(200).send({ link, newItem });
      }
      return res.status(500).send({ status: 500, message: 'Error writing new image ...' });
    }
    return res.status(500).send({ status: 500, message: 'No such course ...' });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};
