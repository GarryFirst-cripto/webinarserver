import { Request, Response } from 'express';
import userRepository from '../../repositories/usersRepository';
import { env } from '../../config/dataConfig';
import { createAccessToken, createRefreshToken } from '../helpers/cryptoHelper';
import { IListFilter, IRequest } from '../interfaces/interfaces';
import sendSMS from '../helpers/smsHelper';
import { host, setHost } from '../helpers/addressHelper';
import * as imageService from './imageServices';

export const getUsers = async (req: IRequest, res: Response) => {
  const { adminMode } = env.admin;
  if (req.user.mode === adminMode) {
    const filter = <IListFilter> <unknown>req.query;
    const list = await userRepository.getList(filter);
    return res.status(200).send(list);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

// (<IRequest> req).user.id, (<IRequest> req).user.mode)
export const getUserById = async (req: Request, res: Response) => {
  const { id: userId, mode } = (<IRequest> req).user;
  if (mode >= 0) {
    const user = await userRepository.getUserById(userId);
    return res.status(200).send(user);
  }
  return res.status(401).send({ status: 401, message: 'Incorrect token. Try again ...' });
}

export const refresh = async (req: Request, res: Response) => {
  const { user: { id, reff } } = <IRequest> req;
  if (reff >= 0) {
    const accessToken = createAccessToken({ id, mode: reff });
    return res.status(200).send({ accessToken });
  }
  return res.status(403).send({ status: 403, message: 'Incorrect token. Try again ...' });
};

export const register = async (req: Request, res: Response) => {
  const user = await userRepository.create(req.body);
  const { id } = user;
  if (id) {
    const accessToken = createAccessToken({ id, mode: 0 });
    const refreshToken = createRefreshToken({ id, mode: 0 });
    return res.status(200).send({ refreshToken, accessToken, user });
  }
  return res.status(406).send({ status: 406, message: 'User`s data not acceptable ...' });
}

export const login = async (req: Request, res: Response) => {
  const { phone, code, repeat } = req.body;
  if (phone && phone !== '') {
    const user = await userRepository.getByPhone(phone);
    if (user) {
      if (code) {
        if (user.verify === code) {
          const { id } = user;
          const accessToken = createAccessToken({ id, mode: 0 });
          const refreshToken = createRefreshToken({ id, mode: 0 });
          await userRepository.updateById(user.id, { verify: '', confirmed: true });
          return res.status(200).send({ refreshToken, accessToken, user });
        }
        return res.status(403).send({ status: 403, message: 'Incorrect code. Try again ...' });
      }
      // eslint-disable-next-line no-shadow
      // const vCode = repeat ? user.verify : Math.round(1000 + 9000 * Math.random()).toString();
      const vCode = '1234';
      await userRepository.updateById(user.id, { verify: vCode });
      // const result: any = await sendSMS(phone, vCode);
      const result: any = "SM5b3d977b595d4af9bbf8c10dcde7eb92";
      if (result.error) {
        return res.status(403).send({ status: 403, message: result.error });
      } else {
        return res.status(200).send({ status: 200, result });
      }
    }
    if (!code) {
      await register(req, res);
      return;
    }
  }
  return res.status(404).send({ status: 404, message: 'No such phone ...' });
}

export const updateUser = async (req: IRequest, res: Response) => {
  const { id: updId } = req.body;
  const userId = updId || req.user.id;
  const { adminMode } = env.admin;
  if (req.user.mode === adminMode || req.user.id === userId) {
    const result = await userRepository.updateById(userId, req.body);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const deleteUser = async (req: IRequest, res: Response) =>  {
  const id = req.params.id;
  const { adminMode } = env.admin;
  if (req.user.mode === adminMode || req.user.id === id) {
    const result = await userRepository.delete(id);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export interface IPwd {
  pwd: string
}

export const doAdminAuth = async (PWD: IPwd = { pwd: '' }) => {
  const { pwd } = PWD;
  const { adminName, adminPWD, adminMode } = env.admin;
  if (pwd === adminPWD) {
    let admin = await userRepository.getByName(adminName);
    if (!admin) {
      admin = await userRepository.create({ username: adminName, phone: 'no-admin-phone' });
    }
    const { id } = admin;
    const accessToken = createAccessToken({ id, mode: adminMode });
    const refreshToken = createRefreshToken({ id, mode: adminMode });
    return { refreshToken, accessToken, user: admin };
  }
  return { status: 403, message: 'Incorrect admin password given. Try again ...' };
}

// export const postImage = async (req: Request, res: Response) => {
//   const { adminMode } = env.admin;
//   if ((<IRequest>req).user.mode === adminMode) {
//     setHost(req.headers.host);
//     const { file: { originalname, buffer } } = req;
//     const { params: { id } } = req;
//     const user = await userRepository.getById(id);
//     if (user) {
//       if (user.link) await imageService.deleteImage(user.link);
//       const { fileId } = await imageService.uploadFile(buffer, originalname || 'image.jpg');
//       if (fileId) {
//         const link = `${host}${fileId}`;
//         const newUser = await userRepository.updateById(id, { link });
//         return res.status(200).send({ link, newUser });
//       }
//       return res.status(500).send({ status: 500, message: 'Error writing new image ...' });
//     }
//     return res.status(500).send({ status: 500, message: 'No such user ...' });
//   }
//   return res.status(403).send({ status: 403, message: 'Access denied ...' });
// };
