import { Request, Response } from 'express';
import schedulesRepository from '../../repositories/schedulesRepository';
import { host, setHost } from '../helpers/addressHelper';
// import { env } from '../../config/dataConfig';
import { IRequest, IListFilter } from '../interfaces/interfaces';
import * as imageService from './imageServices';

export const getSchedulesList = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const filter = <IListFilter> <unknown>req.query;
    const list = await schedulesRepository.getList(filter);
    return res.status(200).send(list);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const getScheduleById = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const id = req.params.id;
    const result = await schedulesRepository.getById(id);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const createSchedule = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const result = await schedulesRepository.create(req.body);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const updateSchedule = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const { id } = req.body;
    const result = await schedulesRepository.updateById(id, req.body);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const deleteSchedule = async (req: Request, res: Response) =>  {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    const id = req.params.id;
    const item = await schedulesRepository.getById(id);
    let image: boolean = false;
    if (item) {
      const { link } = item;
      image = await imageService.deleteImage(link);
    }
    const { result } = await schedulesRepository.delete(id);
    return res.status(200).send({ result, image });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
}

export const deleteScheduleItemLocal = async (id: string) =>  {
  const item = await schedulesRepository.getById(id);
  let image: boolean = false;
  if (item) {
    const { link } = item;
    image = await imageService.deleteImage(link);
  }
  const { result } = await schedulesRepository.delete(id);
  return { result, image };
}

export const postImage = async (req: Request, res: Response) => {
  const { user: { mode } } = <IRequest> req;
  if (mode >= 0) {
    setHost(req.headers.host);
    const { file: { originalname, buffer } } = req;
    const { params: { id } } = req;
    const item = await schedulesRepository.getById(id);
    if (item) {
      if (item.link) await imageService.deleteImage(item.link);
      const { fileId } = await imageService.uploadFile(buffer, originalname || 'image.jpg');
      if (fileId) {
        const link = `${host}${fileId}`;
        const schedule = await schedulesRepository.updateById(id, { link });
        return res.status(200).send({ link, schedule });
      }
      return res.status(500).send({ status: 500, message: 'Error writing new image ...' });
    }
    return res.status(500).send({ status: 500, message: 'No such course ...' });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};
