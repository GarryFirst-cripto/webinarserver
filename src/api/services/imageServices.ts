import { google } from 'googleapis';
import { Readable } from 'stream';
import googleClient from '../helpers/googleDiskHelper';
import asyncForEach from '../helpers/asyncHelper';
import { host } from '../helpers/addressHelper';

export async function loadImage(fileId: string) {
  if (!fileId || fileId === 'undefined') {
    return {};
  }
  const auth = googleClient();
  const drive = google.drive({ version: 'v3', auth });
  const load = new Promise((resolve, reject) => drive.files.get({
    fileId,
    alt: 'media'
  }, { responseType: 'arraybuffer' }, (err, { data }) => {
    if (err) {
      reject(err);
    } else {
      const ress = Buffer.from(<any> data);
      resolve(ress);
    }
  }));
  const result = await load;
  return result;
}

export async function uploadFile(fileBlob: ArrayBuffer, storeName: string) {
  const stream = new Readable();
  // eslint-disable-next-line no-underscore-dangle
  stream._read = () => {};
  stream.push(fileBlob);
  stream.push(null);
  const auth = googleClient();
  const drive = google.drive({ version: 'v3', auth });
  const fileMetadata = { name: storeName };
  const media = { mimeType: 'image/jpeg', body: stream };
  const file = await drive.files.create(<any> {
    includePermissionsForView: 'published',
    resource: fileMetadata,
    media
  });
  const { data } = file;
  if (data) {
    const { id: fileId } = data;
    drive.permissions.create({ fileId, requestBody: { role: 'reader', type: 'anyone' } });
    const webViewLink = await drive.files.get({ fileId, fields: 'webViewLink' });
    if (webViewLink) {
      const { data: { webViewLink: link } } = webViewLink;
      // data.link = link;
      return { fileId, link }
    }
  }
  return {};
}

export async function deleteImage(fileId: string): Promise<boolean> {
  if (fileId && fileId.startsWith(host)) {
    const i = fileId.lastIndexOf('/');
    if (i >= 0) {
      // eslint-disable-next-line no-param-reassign
      fileId = fileId.substr(i + 1);
    }
    const auth = googleClient();
    const drive = google.drive({ version: 'v3', auth });
    try {
      const dell = new Promise((resolve, reject) => drive.files.delete({ fileId }, err => {
        if (err) {
          reject(false);
        } else {
          resolve(true);
        }
      })).catch(() => false);
      const result = <boolean> await dell;
      return result;
    } catch {
      return false;
    }
  }
  return true;
}

export async function deleteImageList(fileList: string) {
  if (fileList) {
    const list = fileList.split(',');
    await asyncForEach(async (item: string) => {
      await deleteImage(item);
    }, list);
  }
}
