import { Router } from 'express';
import * as scheduleService from '../services/scheduleServices';
import scheduleMiddleware from '../middlewares/schedulesMiddleware';
import fileMiddleware from '../middlewares/fileMiddleware';

const router = Router();

router
  .get('/:id', (req, res, next) => scheduleService.getScheduleById(req, res)
    .catch(next))
  .get('/', (req, res, next) => scheduleService.getSchedulesList(req, res)
    .catch(next))
  .post('/image/:id', fileMiddleware, (req, res, next) => scheduleService.postImage(req, res)
    .catch(next))
  .post('/', scheduleMiddleware, (req, res, next) => scheduleService.createSchedule(req, res)
    .catch(next))
  .put('/', (req, res, next) => scheduleService.updateSchedule(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => scheduleService.deleteSchedule(req, res)
    .catch(next));

export default router;