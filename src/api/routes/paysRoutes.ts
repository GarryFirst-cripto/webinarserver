import { Router } from 'express';
import * as paysService from '../services/paysServices';
import payMiddleware from '../middlewares/payMiddleware';

const router = Router();

router
  .get('/:id', (req, res, next) => paysService.getPayById(req, res)
    .catch(next))
  .get('/', (req, res, next) => paysService.getPaysList(req, res)
    .catch(next))
  .post('/', payMiddleware, (req, res, next) => paysService.createPay(req, res)
    .catch(next))
  .put('/', (req, res, next) => paysService.updatePay(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => paysService.deletePay(req, res)
    .catch(next));

export default router;