import { Router } from 'express';
import * as coursesService from '../services/coursesServices';
import { IListFilter } from '../interfaces/interfaces';
import courseMiddleware from '../middlewares/coursesMiddleware';
import courseUpdateMiddleware from '../middlewares/coursesUpdateMiddleware';
import fileMiddleware from '../middlewares/fileMiddleware';

const router = Router();

router
  .get('/tags/:id', (req, res, next) => coursesService.getCoursesByTags(req, res)
    .catch(next))
  .get('/find/:id', (req, res, next) => coursesService.getCoursesByText(req, res)
    .catch(next))
  .get('/faculty/:id', (req, res, next) => coursesService.getCoursesByFaculty(req, res)
    .catch(next))
  .get('/full', (req, res, next) => coursesService.getCoursesFullList(req, res)
    .catch(next))
  .get('/:id', (req, res, next) => coursesService.getCourseById(req, res)
    .catch(next))
  .get('/', (req, res, next) => coursesService.getCoursesList(req, res)
    .catch(next))
  .post('/image/:id', fileMiddleware, (req, res, next) => coursesService.postImage(req, res)
    .catch(next))
  .post('/', courseMiddleware, (req, res, next) => coursesService.createCourse(req, res)
    .catch(next))
  .put('/faculty', courseUpdateMiddleware, (req, res, next) => coursesService.joinCourseFaculty(req, res)
    .catch(next))
  .put('/exitfaculty', courseUpdateMiddleware, (req, res, next) => coursesService.exitCourseFaculty(req, res)
    .catch(next))
  .put('/', courseUpdateMiddleware, (req, res, next) => coursesService.updateCourse(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => coursesService.deleteCourse(req, res)
    .catch(next));

export default router;