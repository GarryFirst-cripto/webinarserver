import { Router } from 'express';
import * as coursesItemService from '../services/coursesItemServices';
import courseItemMiddleware from '../middlewares/coursesItemMiddleware';
import fileMiddleware from '../middlewares/fileMiddleware';

const router = Router();

router
  .get('/:id', (req, res, next) => coursesItemService.getCourseItemById(req, res)
    .catch(next))
  .get('/', (req, res, next) => coursesItemService.getCourseItemsList(req, res)
    .catch(next))
  .post('/image/:id', fileMiddleware, (req, res, next) => coursesItemService.postImage(req, res)
    .catch(next))
  .post('/', courseItemMiddleware, (req, res, next) => coursesItemService.createCourseItem(req, res)
    .catch(next))
  .put('/', (req, res, next) => coursesItemService.updateCourseItem(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => coursesItemService.deleteCourseItem(req, res)
    .catch(next));

export default router;