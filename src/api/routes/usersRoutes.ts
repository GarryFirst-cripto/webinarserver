import { Router } from 'express';
import * as userService from '../services/usersServices';
import { IRequest, IListFilter } from '../interfaces/interfaces';
import registrMiddleware from '../middlewares/registrMiddleware';
import updateMiddleware from '../middlewares/updateMiddleware';
import fileMiddleware from '../middlewares/fileMiddleware';

const router = Router();

router
  .get('/refresh', (req, res, next) => userService.refresh(req, res)
    .catch(next))
  .get('/user', (req, res, next) => userService.getUserById(req, res)
    .catch(next))
  .get('/admin/auth', (req, res, next) => userService.doAdminAuth(<userService.IPwd> <unknown>req.query)
    .then(data => res.send(data))
    .catch(next))
  .get('/:id', (req, res, next) => userService.getUserById(req, res)
    .catch(next))
  .get('/', (req, res, next) => userService.getUsers(<IRequest>req, res)
    .catch(next))
  .post('/register', registrMiddleware, (req, res, next) => userService.register(req, res)
    .catch(next))
  .post('/login', (req, res, next) => userService.login(req, res)
    .catch(next))
  // .post('/image/:id', fileMiddleware, (req, res, next) => userService.postImage(req, res)
  //   .catch(next))
  .put('/', updateMiddleware, (req, res, next) => userService.updateUser(<IRequest>req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => userService.deleteUser(<IRequest> <unknown>req, res)
    .catch(next));

export default router;
