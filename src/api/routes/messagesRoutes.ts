import { Router } from 'express';
import * as messagesService from '../services/messagesService';
import messageMiddleware from '../middlewares/messageMiddleware';

const router = Router();

router
  .get('/course/:id', (req, res, next) => messagesService.getMessageByCourse(req, res)
    .catch(next))
  .get('/:id', (req, res, next) => messagesService.getMessageById(req, res)
    .catch(next))
  .get('/', (req, res, next) => messagesService.getMessagesList(req, res)
    .catch(next))
  .post('/', messageMiddleware, (req, res, next) => messagesService.createMessage(req, res)
    .catch(next))
  .put('/', (req, res, next) => messagesService.updateMessage(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => messagesService.deleteMessage(req, res)
    .catch(next));

export default router;