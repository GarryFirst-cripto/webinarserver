import { Router } from 'express';
import * as subscriptService from '../services/subscriptServices';
import subMiddleware from '../middlewares/subMiddleware';

const router = Router();

router
  .get('/user/:id', (req, res, next) => subscriptService.getSubsByUserId(req, res)
    .catch(next))
  .get('/course/:id', (req, res, next) => subscriptService.getSubsByCourseId(req, res)
    .catch(next))
  .get('/:id', (req, res, next) => subscriptService.getSubsById(req, res)
    .catch(next))
  .get('/', (req, res, next) => subscriptService.getSubsList(req, res)
    .catch(next))
  .post('/', subMiddleware, (req, res, next) => subscriptService.createSub(req, res)
    .catch(next))
  .put('/', (req, res, next) => subscriptService.updateSub(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => subscriptService.deleteSub(req, res)
    .catch(next));

export default router;