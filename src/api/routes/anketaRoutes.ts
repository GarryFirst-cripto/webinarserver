import { Router } from 'express';
import * as anketaService from '../services/anketaServices';
import anketaMiddleware from '../middlewares/anketaMiddleware';

const router = Router();

router
  .get('/:id', (req, res, next) => anketaService.getAnketaById(req, res)
    .catch(next))
  .get('/', (req, res, next) => anketaService.getAnketaList(req, res)
    .catch(next))
  .post('/', anketaMiddleware, (req, res, next) => anketaService.createAnketa(req, res)
    .catch(next))
  .put('/', (req, res, next) => anketaService.updateAnketa(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => anketaService.deleteAnketa(req, res)
    .catch(next));

export default router;