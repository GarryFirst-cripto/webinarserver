import { Router } from 'express';
import * as facultyService from '../services/facultyServices';
import { IListFilter } from '../interfaces/interfaces';
import facultyMiddleware from '../middlewares/facultyMiddleware';
import facultyUpdateMiddleware from '../middlewares/facultyUpdateMiddleware';

const router = Router();

router
  .get('/full', (req, res, next) => facultyService.getFacultiesFullList(req, res)
    .then(list => res.send(list))
    .catch(next))
  .get('/:id', (req, res, next) => facultyService.getFacultyById(req, res)
    .catch(next))
  .get('/', (req, res, next) => facultyService.getFacultiesList(req, res)
    .catch(next))
  .post('/', facultyMiddleware, (req, res, next) => facultyService.createFaculty(req, res)
    .catch(next))
  .put('/', facultyUpdateMiddleware, (req, res, next) => facultyService.updateFaculty(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => facultyService.deleteFaculty(req, res)
    .catch(next));

export default router;
