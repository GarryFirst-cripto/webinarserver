import { Router } from 'express';
import * as reviewService from '../services/reviewsServices';
import reviewMiddleware from '../middlewares/reviewMiddleware';
import fileMiddleware from '../middlewares/fileMiddleware';

const router = Router();

router
  .get('/:id', (req, res, next) => reviewService.getReviewById(req, res)
    .catch(next))
  .get('/', (req, res, next) => reviewService.getReviewsList(req, res)
    .catch(next))
  .post('/image/:id', fileMiddleware, (req, res, next) => reviewService.postImage(req, res)
    .catch(next))
  .post('/', reviewMiddleware, (req, res, next) => reviewService.createReview(req, res)
    .catch(next))
  .put('/', (req, res, next) => reviewService.updateReview(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => reviewService.deleteReview(req, res)
    .catch(next));

export default router;