import { Router } from 'express';
import * as tagsService from '../services/tagsServices';

const router = Router();

router
  .get('/:id', (req, res, next) => tagsService.getTagById(req, res)
    .catch(next))
  .get('/', (req, res, next) => tagsService.getTagsList(req, res)
    .catch(next))
  .post('/', (req, res, next) => tagsService.createTag(req, res)
    .catch(next))
  .put('/', (req, res, next) => tagsService.updateTag(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => tagsService.deleteTag(req, res)
    .catch(next));

export default router;