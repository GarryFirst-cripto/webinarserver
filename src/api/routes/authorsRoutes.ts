import { Router } from 'express';
import * as authorService from '../services/authorsServices';
import authorMiddleware from '../middlewares/authorMiddleware';
import fileMiddleware from '../middlewares/fileMiddleware';

const router = Router();

router
  .get('/:id', (req, res, next) => authorService.getAuthorById(req, res)
    .catch(next))
  .get('/', (req, res, next) => authorService.getAuthorsList(req, res)
    .catch(next))
  .post('/image/:id', fileMiddleware, (req, res, next) => authorService.postImage(req, res)
    .catch(next))
  .post('/', authorMiddleware, (req, res, next) => authorService.createAuthor(req, res)
    .catch(next))
  .put('/', (req, res, next) => authorService.updateAuthor(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => authorService.deleteAuthor(req, res)
    .catch(next));

export default router;