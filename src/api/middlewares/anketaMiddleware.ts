import { Request, Response } from 'express';
import usersRepository from '../../repositories/usersRepository';

export default async (req: Request, res: Response, next: () => void): Promise<any> => {
  const { userId } = req.body;
  if (userId) {
    const item = await usersRepository.getById(userId);
    if (!item) {
      return res.status(404).send({ status: 404, message: 'No user with sach id !' });
    }
  } else {
    return res.status(404).send({ status: 404, message: 'No user ID given !' });
  };
  return next();
};