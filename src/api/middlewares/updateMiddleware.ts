import { Request, Response } from 'express';
import { IRequest } from '../interfaces/interfaces';
import userRepository from '../../repositories/usersRepository';

export default async (req: Request, res: Response, next: () => void) => {
  const { user: { id } } = <IRequest> req;
  const idd = req.body.id || id;
  const { username, phone } = req.body;
  if (phone) {
    const userByPhone = await userRepository.getByPhone(phone);
    if (userByPhone && userByPhone.id !== idd) {
      return res.status(403).send({ status: 403, message: 'This Phone is already taken.' });
    }
  };
  if (username) {
    const userByName = await userRepository.getByName(username);
    if (userByName && userByName.id !== idd) {
      return res.status(403).send({ status: 403, message: 'Username is already taken.' });
    }
  };
  next();
};