import jwt from 'jsonwebtoken';
import { Request, Response } from 'express';
import userRepository from '../../repositories/usersRepository';
import { env } from '../../config/dataConfig';
import { IRequest, IToken } from '../interfaces/interfaces';
import { fetchTokenData } from '../helpers/cryptoHelper';

export default (routesWhiteList: any[]) => (req: Request, res: Response, next: () => any) => (
  routesWhiteList.some(route => req.path.startsWith(route))
    ? next()
    : jwtMiddleware(<IRequest> req, res, next) // auth the user if requested path isn't from the white list
);

async function jwtMiddleware(req: IRequest, res: Response, next: () => any) {
  const headers = JSON.parse(JSON.stringify(req.headers));
  const { 'authorization': token } = headers;
  if (token) {
    try {
      const { id, mode, reff } = fetchTokenData(token);
      if (id) {
        const user = await userRepository.getById(id);
        if (user) {
          req.user = { id, mode, reff };
          return next();
        }
      }
    } catch (e) {
      return res.status(401).send({ status: 401, message: 'Unauthorized.' });
    }
  }
  return res.status(401).send({ status: 401, message: 'Unauthorized.' });
};
