import { Request, Response } from 'express';
import coursesRepository from '../../repositories/coursesRepository';

export default async (req: Request, res: Response, next: () => void): Promise<any> => {
  const { name } = req.body;
  if (name) {
    const item = await coursesRepository.getByName(name);
    if (item) {
      return res.status(401).send({ status: 401, message: 'This course name is already taken.' });
    }
  } else {
    return res.status(404).send({ status: 404, message: 'No course name given !' });
  }
  return next();
};