import { Request, Response } from 'express';
import facultyRepository from '../../repositories/facultyRepository';

export default async (req: Request, res: Response, next: () => void) => {
  const { id, faculty } = req.body;
  if (faculty) {
    const item = await facultyRepository.getByName(faculty);
    if (item && item.id !== id) {
      return res.status(403).send({ status: 403, message: 'This faculty name is already taken.' });
    }
  };
  next();
};