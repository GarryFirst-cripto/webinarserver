import { Request, Response } from 'express';
import coursesRepository from '../../repositories/coursesRepository';

export default async (req: Request, res: Response, next: () => void): Promise<any> => {
  const { courseId } = req.body;
  if (courseId) {
    const item = await coursesRepository.getById(courseId);
    if (!item) {
      return res.status(404).send({ status: 404, message: 'No course with sach id !' });
    }
  } else {
    return res.status(404).send({ status: 404, message: 'No course ID given !' });
  }
  return next();
};