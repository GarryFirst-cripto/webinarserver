import { Request, Response } from 'express';
import coursesRepository from '../../repositories/coursesRepository';
import usersRepository from '../../repositories/usersRepository';

export default async (req: Request, res: Response, next: () => void): Promise<any> => {
  const { courseId, userId, authorname } = req.body;
  if (courseId) {
    const item = await coursesRepository.getById(courseId);
    if (!item) {
      return res.status(404).send({ status: 404, message: 'No course with sach id !' });
    }
  } else {
    return res.status(404).send({ status: 404, message: 'No course ID given !' });
  };
  if (userId) {
    const item = await usersRepository.getUserById(userId);
    if (!item) {
      return res.status(404).send({ status: 404, message: 'No user with sach id !' });
    }
    req.body.authorname = item.anket.username;
    return next();
  }
  if (!authorname) {
    return res.status(404).send({ status: 404, message: 'Author name cannot be null !'});
  }
  return next();
};