import { Request, Response } from 'express';
import userRepository from '../../repositories/usersRepository';

export default async (req: Request, res: Response, next: () => void): Promise<any> => {
  const { username, phone } = req.body;
  if (phone) {
    const userByPhone = await userRepository.getByPhone(phone);
    if (userByPhone) {
      return res.status(403).send({ status: 403, message: 'This Phone is already taken.' });
    }
  } else {
    return res.status(404).send({ status: 404, message: 'No Phone given !' });
  }
  if (username) {
    const userByName = await userRepository.getByName(username);
    if (userByName) {
      return res.status(403).send({ status: 403, message: 'Username is already taken.' });
    }
  } else {
    return res.status(404).send({ status: 404, message: 'No Username given !' });
  }
  return next();
};