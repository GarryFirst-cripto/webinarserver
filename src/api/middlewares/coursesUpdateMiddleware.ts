import { Request, Response } from 'express';
import coursesRepository from '../../repositories/coursesRepository';
import usersRepository from '../../repositories/usersRepository';
import course from '../../repositories/models/course';
import { ICourseModel } from "../../repositories/interfaces/interfaces";

export default async (req: Request, res: Response, next: () => void) => {
  const { id, name, moderId } = req.body;
  if (id) {
    const course: ICourseModel = await coursesRepository.getById(id);
    if (!course) {
      return res.status(404).send({ status: 404, message: 'No course with sach id !' });
    }
  } else {
    return res.status(404).send({ status: 404, message: 'No course ID given !' });
  };
  if (name) {
    const item = await coursesRepository.getByName(name);
    if (item && item.id !== id) {
      return res.status(403).send({ status: 403, message: 'This course name is already taken.' });
    }
  };
  if (moderId) {
    const { moderId: curModerId } = <ICourseModel> <unknown>course;
    if (curModerId && curModerId !== moderId) {
      return res.status(403).send({ status: 403, message: 'This course allready moderated.' });
    }
    const item = await usersRepository.getById(moderId);
    if (!item) {
      return res.status(403).send({ status: 403, message: 'No moderator with sach id !' });
    }
    if (item.status !== 'moderator') {
      return res.status(403).send({ status: 403, message: 'This user is not a moderator !' });
    }
  };
  next();
};