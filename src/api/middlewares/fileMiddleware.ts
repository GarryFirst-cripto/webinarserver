import multer from 'multer';
import { env } from '../../config/dataConfig';

const storage = multer.memoryStorage();
const upload = multer({
  storage,
  limits: {
    fileSize: parseInt(env.filesize)
  }
});

export default upload.single('image');
