import { Request, Response } from 'express';
import usersRepository from '../../repositories/usersRepository';
import coursesRepository from '../../repositories/coursesRepository';

export default async (req: Request, res: Response, next: () => void): Promise<any> => {
  const { courseId, userId } = req.body;
  if (courseId) {
    const item = await coursesRepository.getById(courseId);
    if (!item) {
      return res.status(404).send({ status: 404, message: 'No course with sach id !' });
    }
  } else {
    return res.status(404).send({ status: 404, message: 'No course ID given !' });
  };
  if (userId) {
    const item = await usersRepository.getById(userId);
    if (!item) {
      return res.status(404).send({ status: 404, message: 'No user with sach id !' });
    }
  } else {
    return res.status(404).send({ status: 404, message: 'No user ID given !' });
  };
  return next();
};