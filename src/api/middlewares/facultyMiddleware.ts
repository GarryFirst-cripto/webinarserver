import { Request, Response } from 'express';
import facultyRepository from '../../repositories/facultyRepository';

export default async (req: Request, res: Response, next: () => void): Promise<any> => {
  const { faculty } = req.body;
  if (faculty) {
    const item = await facultyRepository.getByName(faculty);
    if (item) {
      return res.status(403).send({ status: 403, message: 'This faculty name is already taken.' });
    }
  } else {
    return res.status(404).send({ status: 404, message: 'No faculty name given !' });
  }
  return next();
};