import { Request } from 'express';

export interface IRequest extends Request {
  user: { 
    id: string,
    mode: number,
    reff?: number
  }
}

export interface IToken {
  id: string;
}

export interface ITokenData {
  id: string,
  mode: number,
  reff?: number
}

export interface IListFilter {
  from: number;
  count: number;
  required?: boolean;
  translations?: number;
  group: string;
  status?: string;
  answer?: string;
}

export interface IUser {
  id: string;
  username: string;
  sex: string;
  age: Date;
  pushId: string;
  status: string;
  sensay: string;
  verify: string;
}

export interface ILinks {
  fileId?: string;
  link?: string;
}
