const { usersSeed } = require('./seedData/usersSeed');
const { facultiesSeed } = require('./seedData/facultiesSeed');

module.exports = {
  up: async queryInterface => {
    try {
      const data = new Date();

      const usersMappedSeed = usersSeed.map(item => ({
        createdAt: data, ...item
      }));
      await queryInterface.bulkInsert('users', usersMappedSeed, {});

      const facultiesMappedSeed = facultiesSeed.map(item => ({
        createdAt: data, ...item
      }));
      await queryInterface.bulkInsert('faculties', facultiesMappedSeed, {});

    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(`Seeding error: ${err}`);
    }
  },

  down: async queryInterface => {
    try {
      await queryInterface.bulkDelete('faculties', null, {});
      await queryInterface.bulkDelete('users', null, {});
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(`Seeding error: ${err}`);
    }
  }

};