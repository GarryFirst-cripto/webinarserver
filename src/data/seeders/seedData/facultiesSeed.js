const facultiesSeed = [
  {
    id: 'ddcd7366-201a-4b88-b3ec-c6ec59af26ab',
    faculty: 'IT - бизнес',
    info: 'Информация про IT - бизнес.'
  },
  {
    id: '369fb097-bfa8-40b7-935f-a6ba89a22fea',
    faculty: 'Бизнес',
    info: 'Общие правила ведения бизнеса.'
  },
  {
    id: '5c9bbfc7-7988-46c3-9768-c547c49ccb24',
    faculty: 'Красота и здоровье',
    info: 'Информация про здоровый образ жизни.'
  },
  {
    id: '55b3c9f0-f2c5-4037-a4d5-4397cf9df7ea',
    faculty: 'Медицина'
  },
  {
    id: 'b33970be-7d47-4983-b51e-640b06854dc5',
    faculty: 'Мотивация'
  },
  {
    id: '9ff1e29c-88ae-40be-bb2b-3db11ecd8525',
    faculty: 'Полиграфический дизайн'
  },
  {
    id: '1a32240e-cd0b-4ebe-9477-a9f82f22963a',
    faculty: 'Программирование'
  },
  {
    id: '14e553b0-a088-4257-9f03-215876ab7df6',
    faculty: 'Путешествия'
  },
  {
    id: '34b5c0e2-bcb0-47a1-ac9a-b1faa1d74dc1',
    faculty: 'Развлечения'
  },
  {
    id: '3def2f44-d939-406a-ba64-8d14458ea898',
    faculty: 'Саморазвитие'
  }
]

module.exports = { facultiesSeed };
