const usersSeed = [
  {
    id: '0461e1a5-df2a-496a-8b01-ba5746ea0c3d',
    username: 'Jhon Garrison',
    phone: '+380663089231',
    info: 'Vebi info info info. Vebi-User info info info.',
    confirmed: true
  },
  {
    id: 'cd4be2b6-9493-4d34-ac7f-da33246dbaaa',
    username: 'Milda Pretty',
    phone: '+380630000000',
    confirmed: true
  },
  {
    id: 'e15ea52e-748b-4144-abd5-c1b4df83ceec',
    username: 'sysAdmin',
    phone: 'no-admin-phone',
    confirmed: true
  },
  {
    id: 'b86dff07-0cae-4b5b-8722-f1f5fdb07edd',
    username: 'Tomas Garrison',
    phone: '+380632188031',
    confirmed: true
  },
  {
    id: 'a52b841e-fd89-46b0-bb5e-069721065f7e',
    username: 'Winona Jhons',
    phone: '+380630002222',
    info: 'Winona Info - info -info - info',
    confirmed: true
  }
]

module.exports = { usersSeed };
