module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
    .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
      queryInterface.createTable('courses', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        name: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: true
        },
        mode: Sequelize.STRING,
        subs: {
          type: Sequelize.BOOLEAN,
          default: false
        },
        popular: {
          type: Sequelize.BOOLEAN,
          default: false
        },
        recommended: {
          type: Sequelize.BOOLEAN,
          default: false
        },
        soon: {
          type: Sequelize.BOOLEAN,
          default: false
        },
        infotytle: Sequelize.STRING,
        info: Sequelize.STRING,
        descripttytle: Sequelize.STRING,
        descript: Sequelize.STRING,
        detailstytle: Sequelize.STRING,
        details: Sequelize.STRING,
        link: Sequelize.STRING,
        tags: Sequelize.STRING,
        price: Sequelize.FLOAT,
        priceHalf: Sequelize.FLOAT,
        priceYear: Sequelize.FLOAT,
        index: Sequelize.INTEGER,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('schedules', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        courseId: {
          type: Sequelize.UUID,
          references: {
            model: 'courses',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        },
        name: Sequelize.STRING,
        infotytle: Sequelize.STRING,
        info: Sequelize.STRING,
        link: Sequelize.STRING,
        text: Sequelize.STRING,
        datatime: Sequelize.DATE,
        duration: Sequelize.FLOAT,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('courseitems', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        courseId: {
          type: Sequelize.UUID,
          references: {
            model: 'courses',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        },
        name: Sequelize.STRING,
        infotytle: Sequelize.STRING,
        info: Sequelize.STRING,
        link: Sequelize.STRING,
        text: Sequelize.STRING,
        index: Sequelize.INTEGER,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('faculties', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        faculty: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: true
        },
        info: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('coursefaculties', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        courseId: {
          allowNull: false,
          type: Sequelize.UUID,
          references: {
            model: 'courses',
            key: 'id'
          }
        },
        facultyId: {
          allowNull: false,
          type: Sequelize.UUID,
          references: {
            model: 'faculties',
            key: 'id'
          }
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.addColumn('users', 'pushtoken', {
        type: Sequelize.STRING
      }, { transaction })
    ]))),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('users', 'pushtoken', { transaction }),
      queryInterface.dropTable('coursefaculties', { transaction }),
      queryInterface.dropTable('facultys', { transaction }),
      queryInterface.dropTable('courseitems', { transaction }),
      queryInterface.dropTable('schedules', { transaction }),
      queryInterface.dropTable('courses', { transaction })
    ]))
};