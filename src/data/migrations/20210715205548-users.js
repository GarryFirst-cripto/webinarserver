module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
    .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
      queryInterface.createTable('users', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        phone: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: true
        },
        username: Sequelize.STRING,
        confirmed: Sequelize.BOOLEAN,
        pays: Sequelize.STRING,
        info: Sequelize.STRING,
        faculties: Sequelize.STRING,
        verify: Sequelize.STRING(10),
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction })
    ]))),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.dropTable('users', { transaction })
    ]))
};
