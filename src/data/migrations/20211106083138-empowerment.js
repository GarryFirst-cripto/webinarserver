module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.createTable('ankets', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        userId: {
          type: Sequelize.UUID,
          references: {
            model: 'users',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        },
        username: Sequelize.STRING,
        email: Sequelize.STRING,
        instagram: Sequelize.STRING,
        instagramcount: Sequelize.INTEGER,
        youtube: Sequelize.STRING,
        youtubecount: Sequelize.INTEGER,
        info: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('messages', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        courseId: {
          type: Sequelize.UUID,
          references: {
            model: 'courses',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        },
        userId: {
          type: Sequelize.UUID,
          references: {
            model: 'users',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        },
        note: Sequelize.STRING,
        text: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.addColumn('users', 'status', {
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.addColumn('users', 'level', {
        type: Sequelize.INTEGER
      }, { transaction }),
      queryInterface.addColumn('authors', 'userId', {
        type: Sequelize.UUID,
        references: {
          model: 'users',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('authors', 'paykoe', {
        type: Sequelize.FLOAT
      }, { transaction }),
      queryInterface.addColumn('courses', 'moderId', {
        type: Sequelize.UUID,
        references: {
          model: 'users',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('courses', 'approved', {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      }, { transaction }),
      queryInterface.addColumn('courses', 'approvedAt', {
        type: Sequelize.DATE
      }, { transaction }),
      queryInterface.addColumn('courseitems', 'approved', {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      }, { transaction }),
      queryInterface.addColumn('schedules', 'approved', {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      }, { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('schedules', 'approved', { transaction }),
      queryInterface.removeColumn('courseitems', 'approved', { transaction }),
      queryInterface.removeColumn('courses', 'approvedAt', { transaction }),
      queryInterface.removeColumn('courses', 'approved', { transaction }),
      queryInterface.removeColumn('courses', 'moderId', { transaction }),
      queryInterface.removeColumn('authors', 'userId', { transaction }),
      queryInterface.removeColumn('authors', 'paykoe', { transaction }),
      queryInterface.removeColumn('users', 'level', { transaction }),
      queryInterface.removeColumn('users', 'status', { transaction }),
      queryInterface.dropTable('messages', { transaction }),
      queryInterface.dropTable('ankets', { transaction })
    ]))
};
