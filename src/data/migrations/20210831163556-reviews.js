module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.createTable('reviews', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        courseId: {
          type: Sequelize.UUID,
          references: {
            model: 'courses',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        },
        username: {
          type: Sequelize.STRING,
          allowNull: false
        },
        userinfo: Sequelize.STRING,
        link: Sequelize.STRING,
        text: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('authors', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        courseId: {
          type: Sequelize.UUID,
          references: {
            model: 'courses',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        },
        authorname: {
          type: Sequelize.STRING,
          allowNull: false
        },
        authorinfo: Sequelize.STRING,
        link: Sequelize.STRING,
        text: Sequelize.STRING,
        index: Sequelize.INTEGER,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.addColumn('courses', 'pro', {
        type: Sequelize.BOOLEAN
      }, { transaction }),
      queryInterface.addColumn('courses', 'trailer', {
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.addColumn('courseitems', 'trailer', {
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.addColumn('schedules', 'trailer', {
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.addColumn('schedules', 'free', {
        type: Sequelize.BOOLEAN
      }, { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('schedules', 'free', { transaction }),
      queryInterface.removeColumn('schedules', 'trailer', { transaction }),
      queryInterface.removeColumn('courseitems', 'trailer', { transaction }),
      queryInterface.removeColumn('courses', 'trailer', { transaction }),
      queryInterface.removeColumn('courses', 'pro', { transaction }),
      queryInterface.dropTable('authors', { transaction }),
      queryInterface.dropTable('reviews', { transaction })
    ]))
};
