import dotenv from 'dotenv'
import * as pgStringParser from 'pg-connection-string'
import { Dialect } from 'sequelize/types'

dotenv.config()

const herokuData = process.env.DATABASE_URL
  ? pgStringParser.parse(process.env.DATABASE_URL)
  : { database: '', user: '', password: '', host: '' }

const dialOptions = process.env.DATABASE_URL
  ? { ssl: { rejectUnauthorized: false }}
  : {}

export const env = {
  app: {
    port: process.env.APP_PORT
  },
  db: {
    database: herokuData.database || process.env.DB_NAME,
    username: herokuData.user || process.env.DB_USERNAME,
    password: herokuData.password || process.env.DB_PASSWORD,
    host: herokuData.host || process.env.DB_HOST,
    port:
      <number>(<unknown>herokuData.port) ||
      <number>(<unknown>process.env.DB_PORT),
    dialect: <Dialect>process.env.DB_DIALECT || 'postgres',
    ssl: true,
    dialectOptions: dialOptions,
    logging: false,
  },
  admin: {
    adminName: process.env.SYSTEM_ADMIN_NAME,
    adminPWD: process.env.SYSTEM_ADMIN_PWD,
    adminMode: 1
  },
  smss: {
    accountSid: process.env.TWILIO_ACCOUNT_SID,
    authToken: process.env.TWILIO_AUTH_TOKEN
  },
  aws: {
    accessKeyId: process.env.AMAZON_ID,
    secretAccessKey: process.env.AMAZON_SECRET,
    bucketName: process.env.AMAZON_BUCKET
  },
  crypto: {
    secretKey: process.env.SECRET_KEY,
    secretExp: process.env.SECRET_EXP
  },
  filesize: process.env.MAX_FILE_SIZE,
  port: process.env.PORT,
  use_env_variable: process.env.DATABASE_URL
}

export const { database, username, password, host, port, dialect, ssl, dialectOptions, logging} = env.db;
