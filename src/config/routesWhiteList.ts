export default [
  '/api/users/signin',
  '/api/users/register',
  '/api/users/login',
  '/api/users/smsreset',
  '/api/users/admin/auth',
  '/image'
];