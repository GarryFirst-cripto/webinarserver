import { UserModel, PayModel, AnketaModel } from './models/index';
import BaseRepository from './baseRepository';
import { IListFilter } from '../api/interfaces/interfaces';

class UserRepository extends BaseRepository {
  model: any;

  async getList(filter: IListFilter) {
    const { from: offset, count: limit, ...rest } = filter;
    const where = { ...rest };
    return await this.model.findAll({
      where, 
      order:['username'],
      include: [{
        model: PayModel
      },{
        model: AnketaModel,
        as: 'anketa'
      }],
      offset, 
      limit
    });
  }

  async getUserById(id: string) {
    return await this.model.findOne({
      where: { id },
      include: [{
        model: PayModel
      },{
        model: AnketaModel,
        as: 'anketa'
      }],
    });
  }

  async getByPhone(phone: string) {
    return await this.model.findOne({ where: { phone } });
  }

  async getByName(username: string) {
    return await this.model.findOne({ where: { username } });
  }
  
  async delete(id: string) {
    const pays = await PayModel.destroy({ where: { userId: id }});
    const anketas = await AnketaModel.destroy({ where: { userId: id }});
    const result = await this.model.destroy({ where: { id } });
    return { result, pays, anketas };
  }

}

export default new UserRepository(UserModel);
