import { AuthorModel, UserModel, AnketaModel } from './models/index';
import BaseRepository from './baseRepository';
import { IListFilter } from '../api/interfaces/interfaces';
import asyncForEach from '../api/helpers/asyncHelper';

class AuthorsRepository extends BaseRepository {
  model: any;

  async getList(filter: IListFilter) {
    const { from: offset, count: limit, ...rest } = filter;
    const where = { ...rest };
    return await this.model.findAll({
      where,
      include: [{
        model: UserModel,
        include: {
          model: AnketaModel
        }
      }],
      offset, 
      limit
    });
  }

  async getById(id: string) {
    const result = await this.model.findOne({
      where: { id },
      include: [{
        model: UserModel,
        include: {
          model: AnketaModel
        }
      }],
    });
    return result;
  }

  async setList(courseId: string, list: any[]) {
    if (list) {
      await this.model.destroy({ where: { courseId } });
      await asyncForEach(async (item: any) => {
        await this.create({ ...item, courseId })
      }, list);
    }
  }

  async deleteCourse(courseId: string) {
    const result = await this.model.destroy({ where: { courseId } });
    return { result };
  }

  async delete(id: string) {
    const result = await this.model.destroy({ where: { id } });
    return { result };
  }

}

export default new AuthorsRepository(AuthorModel);
