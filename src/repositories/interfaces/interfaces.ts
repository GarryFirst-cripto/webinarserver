import { DataTypes } from "sequelize";

interface IIdField {
  allowNull: boolean; 
  type: DataTypes.AbstractDataTypeConstructor;
};

interface IStringField {
  allowNull: boolean; 
  type: DataTypes.StringDataTypeConstructor; 
  unique?: boolean;
}

interface IBooleadField {
  type: DataTypes.AbstractDataTypeConstructor;
  defaultValue: false;
}

export interface IUserModel { 
  phone: IStringField;
  username: DataTypes.StringDataTypeConstructor;
  confirmed: DataTypes.AbstractDataTypeConstructor,
  info: DataTypes.StringDataTypeConstructor;
  faculties: any;
  // faculties: {
  //   type: DataTypes.StringDataTypeConstructor;
  //   get(): string;
  //   set(value: any): void;
  // }; 
  status: DataTypes.StringDataTypeConstructor;
  level: DataTypes.NumberDataTypeConstructor;
  verify: DataTypes.StringDataTypeConstructor;
  pushtoken: DataTypes.StringDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor; 
}

export interface IPayModel { 
  userId: IIdField,
  info: DataTypes.StringDataTypeConstructor;
  paydata: DataTypes.StringDataTypeConstructor;
  index: DataTypes.NumberDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor; 
}

export interface IAnketaModel { 
  userId: IIdField,
  username: DataTypes.StringDataTypeConstructor;
  email: DataTypes.StringDataTypeConstructor;
  instagram: DataTypes.StringDataTypeConstructor;
  instagramcount: DataTypes.NumberDataTypeConstructor,
  youtube: DataTypes.StringDataTypeConstructor;
  youtubecount: DataTypes.NumberDataTypeConstructor,
  info: DataTypes.StringDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor; 
}

export interface ISubscriptModel { 
  userId: IIdField,
  courseId: IIdField,
  summa: DataTypes.FloatDataTypeConstructor;
  info: DataTypes.StringDataTypeConstructor;
  paydata: DataTypes.StringDataTypeConstructor;
  start: DataTypes.DateDataTypeConstructor; 
  finish: DataTypes.DateDataTypeConstructor; 
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor; 
}

export interface ICourseModel {
  name: IStringField;
  mode: DataTypes.StringDataTypeConstructor;
  moderId: DataTypes.AbstractDataTypeConstructor;
  approved: IBooleadField;
  approvedAt: DataTypes.DateDataTypeConstructor,
  subs: IBooleadField;
  popular: IBooleadField;
  recommended: IBooleadField;
  soon: IBooleadField; 
  pro: IBooleadField; 
  infotytle: DataTypes.StringDataTypeConstructor;
  info: DataTypes.StringDataTypeConstructor;
  descripttytle: DataTypes.StringDataTypeConstructor;
  descript: DataTypes.StringDataTypeConstructor;
  detailstytle: DataTypes.StringDataTypeConstructor;
  details: DataTypes.StringDataTypeConstructor;
  link: DataTypes.StringDataTypeConstructor;
  trailer: DataTypes.StringDataTypeConstructor;
  tags: DataTypes.StringDataTypeConstructor;
  price: DataTypes.FloatDataTypeConstructor;
  priceHalf: DataTypes.FloatDataTypeConstructor;
  priceYear: DataTypes.FloatDataTypeConstructor;
  index: DataTypes.NumberDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor;
  updatedAt: DataTypes.DateDataTypeConstructor;
}

export interface IScheduleModel {
  courseId: IIdField,
  approved: IBooleadField;
  free: IBooleadField; 
  name: DataTypes.StringDataTypeConstructor;
  infotytle: DataTypes.StringDataTypeConstructor;
  info: DataTypes.StringDataTypeConstructor;
  link: DataTypes.StringDataTypeConstructor;
  trailer: DataTypes.StringDataTypeConstructor;
  datatime: DataTypes.DateDataTypeConstructor;
  duration: DataTypes.FloatDataTypeConstructor,
  text: DataTypes.StringDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor;
  updatedAt: DataTypes.DateDataTypeConstructor;
}

export interface ICourseItemModel {
  courseId: IIdField,
  approved: IBooleadField;
  name: DataTypes.StringDataTypeConstructor;
  infotytle: DataTypes.StringDataTypeConstructor;
  info: DataTypes.StringDataTypeConstructor;
  link: DataTypes.StringDataTypeConstructor;
  trailer: DataTypes.StringDataTypeConstructor;
  text: DataTypes.StringDataTypeConstructor;
  index: DataTypes.NumberDataTypeConstructor,
  createdAt: DataTypes.DateDataTypeConstructor;
  updatedAt: DataTypes.DateDataTypeConstructor;
}

export interface IReviewModel {
  courseId: IIdField,
  username: IIdField,
  userinfo: DataTypes.StringDataTypeConstructor;
  link: DataTypes.StringDataTypeConstructor;
  text: DataTypes.StringDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor;
  updatedAt: DataTypes.DateDataTypeConstructor;
}

export interface IAuthorModel {
  courseId: IIdField;
  userId: DataTypes.AbstractDataTypeConstructor;
  authorname: IIdField;
  authorinfo: DataTypes.StringDataTypeConstructor;
  link: DataTypes.StringDataTypeConstructor;
  text: DataTypes.StringDataTypeConstructor;
  index: DataTypes.NumberDataTypeConstructor;
  paykoe: DataTypes.FloatDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor;
  updatedAt: DataTypes.DateDataTypeConstructor;
}

export interface IMessageModel {
  courseId: IIdField;
  userId: IIdField,
  note: DataTypes.StringDataTypeConstructor;
  text: DataTypes.StringDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor;
  updatedAt: DataTypes.DateDataTypeConstructor;
}

export interface IFacultyModel {
  faculty: IStringField;
  info: DataTypes.StringDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor;
  updatedAt: DataTypes.DateDataTypeConstructor;
}

export interface ICourceFaculty {
  courseId: { allowNull: boolean; type: DataTypes.AbstractDataTypeConstructor; };
  facultyId: { allowNull: boolean; type: DataTypes.AbstractDataTypeConstructor; };
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor; 
}

export interface ITagModel { 
  tag: DataTypes.StringDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor; 
}
