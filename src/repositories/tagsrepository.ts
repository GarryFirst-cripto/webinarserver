import { TagModel } from './models/index';
import BaseRepository from './baseRepository';
import { IListFilter } from '../api/interfaces/interfaces';
import asyncForEach from '../api/helpers/asyncHelper';

class TagsRepository extends BaseRepository {
  model: any;

  async getList(filter: IListFilter) {
    const { from: offset, count: limit, ...rest } = filter;
    const where = { ...rest };
    return await this.model.findAll({
      where,
      order:['tag'],
      offset, 
      limit
    });
  }

}

export default new TagsRepository(TagModel);
