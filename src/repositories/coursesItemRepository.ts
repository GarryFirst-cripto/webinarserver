import { CourseItemModel } from './models/index';
import BaseRepository from './baseRepository';
import { IListFilter } from '../api/interfaces/interfaces';
import asyncForEach from '../api/helpers/asyncHelper';

class CourseItemRepository extends BaseRepository {
  model: any;

  async getList(filter: IListFilter) {
    const { from: offset, count: limit, ...rest } = filter;
    const where = { ...rest };
    return await this.model.findAll({
      where, 
      offset, 
      limit
    });
  }

  async getByName(name: string) {
    return await this.model.findOne({ where: { name } });
  }
  
  async setList(courseId: string, list: any[]) {
    if (list) {
      await this.model.destroy({ where: { courseId } });
      await asyncForEach(async (item: any) => {
        await this.create({ ...item, courseId })
      }, list);
    }
  }

  async delete(id: string) {
    const result = await this.model.destroy({ where: { id } });
    return { result };
  }

}

export default new CourseItemRepository(CourseItemModel);
