import { DataTypes } from "sequelize";
import { ISubscriptModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: ISubscriptModel, arg2: {}) => any; }) => {
  const Subscript = sequelize.define('subscript', {
    userId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    courseId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    summa: DataTypes.FLOAT,
    info: DataTypes.STRING,
    paydata: DataTypes.STRING,
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Subscript;
};