import { DataTypes } from "sequelize";
import { IReviewModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: IReviewModel, arg2: {}) => any; }) => {
  const Review = sequelize.define('review', {
    courseId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    username: {
      allowNull: false,
      type: DataTypes.STRING
    },
    userinfo: DataTypes.STRING,
    link: DataTypes.STRING,
    text: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Review;
};
