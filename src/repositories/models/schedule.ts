import { DataTypes } from "sequelize";
import { IScheduleModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: IScheduleModel, arg2: {}) => any; }) => {
  const Schedule = sequelize.define('schedule', {
    courseId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    approved: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    free: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    name: DataTypes.STRING,
    infotytle: DataTypes.STRING,
    info: DataTypes.STRING,
    link: DataTypes.STRING,
    trailer: DataTypes.STRING,
    text: DataTypes.STRING,
    datatime: DataTypes.DATE,
    duration: DataTypes.FLOAT,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Schedule;
};
