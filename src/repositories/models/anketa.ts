import { DataTypes } from "sequelize";
import { IAnketaModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: IAnketaModel, arg2: {}) => any; }) => {
  const Anketa = sequelize.define('anket', {
    userId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    instagram: DataTypes.STRING,
    instagramcount: DataTypes.INTEGER,
    youtube: DataTypes.STRING,
    youtubecount: DataTypes.INTEGER,
    info: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Anketa;
};
