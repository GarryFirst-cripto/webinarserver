import { DataTypes } from "sequelize";
import { IUserModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: IUserModel, arg2: {}) => any; }) => {
  const User = sequelize.define('user', {
    phone: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    username: DataTypes.STRING,
    confirmed: DataTypes.BOOLEAN,
    info: DataTypes.STRING,
    faculties: {
      type: DataTypes.STRING,
      get() {
        try {
          return JSON.parse(this.getDataValue('faculties'));
        } catch {
          return null;
        }
      },
      set(value: string) {
        try {
          return this.setDataValue('faculties', JSON.stringify(value));
        } catch {
          return this.setDataValue('faculties', null);
        }
      }
    },
    status: DataTypes.STRING,
    level: DataTypes.INTEGER,
    verify: DataTypes.STRING,
    pushtoken: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return User;
};
