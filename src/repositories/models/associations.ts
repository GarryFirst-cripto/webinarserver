import { AnketaModel, AuthorModel } from ".";

export default (models: { User: any; Pay: any, Anketa: any, Course: any; CourseItem: any; Schedule: any, Faculty: any, Author: any, Review: any, Subscript: any, Message: any }) => {
  const { User, Pay, Anketa, Course, CourseItem, Schedule, Faculty, Author, Review, Subscript, Message } = models;

  User.hasMany(Pay);
  Pay.belongsTo(User);
  User.hasOne(Anketa, { foreignKey: 'userId', as: 'anketa' });
  Anketa.belongsTo(User, { foreignKey: 'userId', as: 'anketa' });
  User.hasMany(Author);
  Author.belongsTo(User);
  Course.hasMany(Schedule);
  Course.hasMany(CourseItem);
  Course.hasMany(Author);
  Course.hasMany(Review);
  Course.belongsTo(User, { foreignKey: 'moderId', as: 'moderator' });
  User.hasMany(Course, { foreignKey: 'moderId', as: 'moderator' });
  Schedule.belongsTo(Course);
  CourseItem.belongsTo(Course);
  Author.belongsTo(Course);
  Review.belongsTo(Course);
  Course.belongsToMany(Faculty, { through: 'coursefaculty' });
  Faculty.belongsToMany(Course, { through: 'coursefaculty' });
  User.hasMany(Subscript);
  Subscript.belongsTo(User);
  Course.hasMany(Subscript);
  Subscript.belongsTo(Course);
  Course.hasMany(Message);
  Message.belongsTo(Course);
  User.hasMany(Message);
  Message.belongsTo(User);
};
