import sequelize from '../../data/db/connection';
import associate from './associations';
import userModel from './user';
import payModel from './pay';
import anketaModel from './anketa';
import courseModel from './course';
import scheduleModel from './schedule';
import courseItemModel from './courseitem';
import facultyModel from './faculty';
import courseToFacultyModel from './coursefaculty';
import authorModel from './author';
import reviewModel from './review';
import subscriptModel from './subscript';
import messageModel from './message';
import tagModel from './tag';

const User = userModel(sequelize);
const Pay = payModel(sequelize);
const Anketa = anketaModel(sequelize);
const Course = courseModel(sequelize);
const Schedule = scheduleModel(sequelize);
const CourseItem = courseItemModel(sequelize);
const Faculty = facultyModel(sequelize);
const CourseToFaculty = courseToFacultyModel(sequelize);
const Author = authorModel(sequelize);
const Review = reviewModel(sequelize);
const Subscript = subscriptModel(sequelize);
const Message = messageModel(sequelize);
const Tag = tagModel(sequelize);

associate({
  User,
  Pay,
  Anketa,
  Course,
  CourseItem,
  Schedule,
  Faculty,
  Author,
  Review,
  Subscript,
  Message
});

export {
  User as UserModel,
  Pay as PayModel,
  Anketa as AnketaModel,
  Course as CourseModel,
  CourseItem as CourseItemModel,
  Schedule as ScheduleModel,
  Faculty as FacultyModel,
  CourseToFaculty as CourseToFacultyModel,
  Author as AuthorModel,
  Review as ReviewModel,
  Subscript as SubscriptModel,
  Message as MessageModel,
  Tag as TagModel
};
