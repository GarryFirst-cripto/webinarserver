import { DataTypes } from "sequelize";
import { IFacultyModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: IFacultyModel, arg2: {}) => any; }) => {
  const Faculty = sequelize.define('faculty', {
    faculty: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    info: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Faculty;
};
