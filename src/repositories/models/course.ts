import { DataTypes } from "sequelize";
import { ICourseModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: ICourseModel, arg2: {}) => any; }) => {
  const Course = sequelize.define('course', {
    name: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    mode: DataTypes.STRING,
    moderId: DataTypes.UUID,
    approved: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    approvedAt: DataTypes.DATE,
    subs: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    popular: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    recommended: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    soon: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    pro: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    infotytle: DataTypes.STRING,
    info: DataTypes.STRING,
    descripttytle: DataTypes.STRING,
    descript: DataTypes.STRING,
    detailstytle: DataTypes.STRING,
    details: DataTypes.STRING,
    link: DataTypes.STRING,
    trailer: DataTypes.STRING,
    tags: DataTypes.STRING,
    price: DataTypes.FLOAT,
    priceHalf: DataTypes.FLOAT,
    priceYear: DataTypes.FLOAT,
    index: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Course;
};
