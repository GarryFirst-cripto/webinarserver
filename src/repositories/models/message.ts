import { DataTypes } from "sequelize";
import { IMessageModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: IMessageModel, arg2: {}) => any; }) => {
  const Message = sequelize.define('message', {
    courseId: {
      allowNull: false,
      type: DataTypes.UUID
    },
    userId: {
      allowNull: false,
      type: DataTypes.UUID
    },
    note: DataTypes.STRING,
    text: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Message;
};