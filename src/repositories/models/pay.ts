import { DataTypes } from "sequelize";
import { IPayModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: IPayModel, arg2: {}) => any; }) => {
  const Pay = sequelize.define('pay', {
    userId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    info: DataTypes.STRING,
    paydata: DataTypes.STRING,
    index: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Pay;
};
