import { DataTypes } from "sequelize";
import { ICourseItemModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: ICourseItemModel, arg2: {}) => any; }) => {
  const CourseItem = sequelize.define('courseitem', {
    courseId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    approved: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    name: DataTypes.STRING,
    infotytle: DataTypes.STRING,
    info: DataTypes.STRING,
    link: DataTypes.STRING,
    trailer: DataTypes.STRING,
    text: DataTypes.STRING,
    index: DataTypes.NUMBER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return CourseItem;
};
