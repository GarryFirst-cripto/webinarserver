import { DataTypes } from "sequelize";
import { IAuthorModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: IAuthorModel, arg2: {}) => any; }) => {
  const Author = sequelize.define('author', {
    courseId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    userId: DataTypes.UUID,
    authorname: {
      allowNull: false,
      type: DataTypes.STRING
    },
    authorinfo: DataTypes.STRING,
    link: DataTypes.STRING,
    text: DataTypes.STRING,
    index: DataTypes.INTEGER,
    paykoe: DataTypes.FLOAT,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Author;
};
