import { DataTypes } from "sequelize";
import { ITagModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: ITagModel, arg2: {}) => any; }) => {
  const Tag = sequelize.define('tag', {
    tag: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Tag;
};
