import { DataTypes } from 'sequelize'
import { ICourceFaculty } from '../interfaces/interfaces'

export default (orm: { define: (arg0: string, arg1: ICourceFaculty , arg2: {}) => any }) => {
  const CourceFaculty = orm.define('coursefaculty', {
    courseId: {
      allowNull: false,
      type: DataTypes.UUID
    },
    facultyId: {
      allowNull: false,
      type: DataTypes.UUID
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  },{},);

  return CourceFaculty;
}