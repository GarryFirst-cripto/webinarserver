import { MessageModel, UserModel } from './models/index';
import BaseRepository from './baseRepository';
import { IListFilter } from '../api/interfaces/interfaces';

class MessagesRepository extends BaseRepository {
  model: any;

  async getList(filter: IListFilter) {
    const { from: offset, count: limit, ...rest } = filter;
    const where = { ...rest };
    return await this.model.findAll({
      where,
      include: {
        model: UserModel
      },
      offset, 
      limit
    });
  }

  async getByCourseId(courseId: string) {
    const result = await this.model.findAll({
      where: { courseId },
      include: {
        model: UserModel
      },
    });
    return result;
  }

  async getById(id: string) {
    const result = await this.model.findOne({
      where: { id },
      include: {
        model: UserModel
      }
    });
    return result;
  }

  async delete(id: string) {
    const result = await this.model.destroy({ where: { id } });
    return { result };
  }

}

export default new MessagesRepository(MessageModel);