import { SubscriptModel, UserModel, CourseModel } from './models/index';
import BaseRepository from './baseRepository';
import { IListFilter } from '../api/interfaces/interfaces';
import asyncForEach from '../api/helpers/asyncHelper';

class SubsRepository extends BaseRepository {
  model: any;

  async getByUser(userId: string) {
    const result = await this.model.findAll({
      where: { userId },
      include: {
        model: CourseModel
      }
    });
    return result;
  }

  async getByCourse(courseId: string) {
    const result = await this.model.findAll({
      where: { courseId },
      include: {
        model: UserModel
      }
    });
    return result;
  }

  async getList(filter: IListFilter) {
    const { from: offset, count: limit, ...rest } = filter;
    const where = { ...rest };
    return await this.model.findAll({
      where,
      include: [{
        model: UserModel
      },{
        model: CourseModel
      }],
      offset, 
      limit
    });
  }

  async getById(id: string) {
    const result = await this.model.findOne({
      where: { id },
      include: [{
        model: UserModel
      },{
        model: CourseModel
      }]
    });
    return result;
  }

  async setList(courseId: string, list: any[]) {
    if (list) {
      await this.model.destroy({ where: { courseId } });
      await asyncForEach(async (item: any) => {
        await this.create({ ...item, courseId })
      }, list);
    }
  }

  async delete(id: string) {
    const result = await this.model.destroy({ where: { id } });
    return { result };
  }

}

export default new SubsRepository(SubscriptModel);
