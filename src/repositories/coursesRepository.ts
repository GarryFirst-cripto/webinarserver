import { Op } from 'sequelize';
import { CourseModel, CourseItemModel, ScheduleModel, FacultyModel, AuthorModel, ReviewModel, UserModel, AnketaModel, MessageModel } from './models/index';
import BaseRepository from './baseRepository';
import { IListFilter } from '../api/interfaces/interfaces';

class CourseRepository extends BaseRepository {
  model: any;

  async getList(filter: IListFilter) {
    const { from: offset, count: limit, ...rest } = filter;
    const where = { ...rest };
    const result = await this.model.findAll({
      where, 
      order:[['index', 'desc'], 'name'],
      include: {
        model:FacultyModel,
        attributes: ['id', 'faculty']
      },
      offset, 
      limit
    });
    result.forEach((item: any) => {
      item.faculties.sort((itemA: any, itemB: any) => itemA.faculty.localeCompare(itemB.faculty));
    });
    return result;
  }

  courseData = [{
        model: UserModel,
        as: 'moderator'
      },{
        model: CourseItemModel,
        order: [['index', 'desc'], 'name'],
      }, {
        model: ScheduleModel
      }, {
        model: FacultyModel
      },{
        model: AuthorModel,
        order: ['index'],
        include: [{
          model: UserModel,
          include: {
            model: AnketaModel,
            as: 'anketa'
          }
        }]
      },{
        model: ReviewModel,
        order: [['createdAt', 'desc']],
      }];

  async getFullList(filter: IListFilter) {
    const { from: offset, count: limit, ...rest } = filter;
    const where = { ...rest };
    const result = await this.model.findAll({
      where, 
      order:[['index', 'desc'], 'name'],
      include: this.courseData,
      offset, 
      limit
    });
    result.forEach((item: any) => {
      item.schedules.sort((itemA: any, itemB: any) => {
        return (new Date(itemA.datatime)).getTime() - (new Date(itemB.datatime)).getTime();
      });
      item.faculties.sort((itemA: any, itemB: any) => itemA.faculty.localeCompare(itemB.faculty));
    });
    return result;
  }

  async getCourseById(id: string) {
    const result = await this.model.findOne({
      where: { id },
      include: [ ... this.courseData,
        {
          model: MessageModel,
          include: {
            model: UserModel
          }
        }
      ]
    });
    if (result) {
      result.schedules.sort((itemA: any, itemB: any) => {
        return (new Date(itemA.datatime)).getTime() - (new Date(itemB.datatime)).getTime();
      })
      result.faculties.sort((itemA: any, itemB: any) => itemA.faculty.localeCompare(itemB.faculty));
    };
    return result;
  }

  async getByName(name: string) {
    return await this.model.findOne({ where: { name } });
  }
  
  async delete(id: string) {
    const result = await this.model.destroy({ where: { id } });
    return { result };
  }

  async getByTags(id: string, filter: IListFilter) {
    const { from: offset, count: limit } = filter;
    const arr = id.replace(' ', '').split(',');
    const tagList: string[] = [];
    arr.forEach(item => {
      tagList.push(`%${item}%`);
    });
    const result = await this.model.findAll({
      where: { tags: { [Op.iLike]: { [Op.any]: tagList } } },
      order:[['index', 'desc'], 'name'],
      include: this.courseData,
      offset,
      limit
    });
    result.forEach((item: any) => {
      item.schedules.sort((itemA: any, itemB: any) => {
        return (new Date(itemA.datatime)).getTime() - (new Date(itemB.datatime)).getTime();
      })
      item.faculties.sort((itemA: any, itemB: any) => itemA.faculty.localeCompare(itemB.faculty));
    });
    return result;
  }

  async getByText(text: string, filter: IListFilter) {
    const { from: offset, count: limit } = filter;
    const fltText = `%${text}%`;
    const result = await this.model.findAll({
      where: { [Op.or]: [{ name: { [Op.iLike]: fltText } }, { tags: { [Op.iLike]: fltText } }] },
      order:[['index', 'desc'], 'name'],
      include: this.courseData,
      offset,
      limit
    });
    result.forEach((item: any) => {
      item.schedules.sort((itemA: any, itemB: any) => {
        return (new Date(itemA.datatime)).getTime() - (new Date(itemB.datatime)).getTime();
      })
      item.faculties.sort((itemA: any, itemB: any) => itemA.faculty.localeCompare(itemB.faculty));
    });
    return result;
  }

  async getByFaculty(id: string, filter: IListFilter) {
    const { from: offset, count: limit, ...rest } = filter;
    const arr = id.replace(' ', '').split(',');
    const result = await this.model.findAll({
      where: { ...rest },
      order:[['index', 'desc'], 'name'],
      include: this.courseData,
      offset,
      limit
    });
    result.forEach((item: any) => {
      item.schedules.sort((itemA: any, itemB: any) => {
        return (new Date(itemA.datatime)).getTime() - (new Date(itemB.datatime)).getTime();
      })
      item.faculties.sort((itemA: any, itemB: any) => itemA.faculty.localeCompare(itemB.faculty));
    });
    return result;
  }

}

export default new CourseRepository(CourseModel);