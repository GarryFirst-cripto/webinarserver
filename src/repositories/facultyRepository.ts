import { FacultyModel, CourseModel, CourseItemModel, ScheduleModel } from './models/index';
import BaseRepository from './baseRepository';
import { IListFilter } from '../api/interfaces/interfaces';

class FacultyRepository extends BaseRepository {
  model: any;

  async getList(filter: IListFilter) {
    const { from: offset, count: limit, ...rest } = filter;
    const where = { ...rest };
    return await this.model.findAll({
      where, 
      order:['faculty'],
      offset, 
      limit
    });
  }

  async getFullList(filter: IListFilter) {
    const { from: offset, count: limit, ...rest } = filter;
    const where = { ...rest };
    const result = await this.model.findAll({
      where, 
      order:['faculty'],
      include: {
        model: CourseModel,
        order: [['index', 'desc'], 'name'],
        include: [{
          model: CourseItemModel,
          order: [['index', 'desc'], 'name'],
        }, {
          model: ScheduleModel
        }]
      },
      offset, 
      limit
    });
    result.forEach((item: any) => {
      item.courses.forEach((subitem: any ) => {
        subitem.schedules.sort((itemA: any, itemB: any) => {
          return (new Date(itemA.datatime)).getTime() - (new Date(itemB.datatime)).getTime();
        })
      });
    });
    return result;
  }

    async getById(id: string) {
    const result = await this.model.findOne({
      where: { id },
      include: {
        model: CourseModel,
        order: [['index', 'desc'], 'name'],
        include: [{
          model: CourseItemModel,
          order: [['index', 'desc'], 'name'],
        }, {
          model: ScheduleModel
        }]
      },
    });
    if (result) {
      result.courses.forEach((item: any) => {
        item.schedules.sort((itemA: any, itemB: any) => {
          return (new Date(itemA.datatime)).getTime() - (new Date(itemB.datatime)).getTime();
        })
      });
    };
    return result;
  }

  async getByName(faculty: string) {
    return await this.model.findOne({ where: { faculty } });
  }
  
  async delete(id: string) {
    const result = await this.model.destroy({ where: { id } });
    return { result };
  }

}

export default new FacultyRepository(FacultyModel);