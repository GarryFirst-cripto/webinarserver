import { CourseToFacultyModel } from './models/index';
import BaseRepository from './baseRepository';
import asyncForEach from '../api/helpers/asyncHelper';

class CourseFacultyRepository extends BaseRepository {
  model: any;

  async getLink(courseId: string, facultyId: string) {
    return await this.model.findOne({ where: { courseId, facultyId } });
  }

  async deleteLink(courseId: string, facultyId: string) {
    const result = await this.model.destroy({ where: { courseId, facultyId } });
    return { result };
  }

  async deleteCourse(courseId: string) {
    const result = await this.model.destroy({ where: { courseId } });
    return { result };
  }

  async setList(courseId: string, list: any[]) {
    if (list) {
      await this.model.destroy({ where: { courseId } });
      await asyncForEach(async (item: any) => {
        await this.create({ ...item, courseId })
      }, list);
    }
  }

  async deleteFaculty(facultyId: string) {
    const result = await this.model.destroy({ where: { facultyId } });
    return { result };
  }

}

export default new CourseFacultyRepository(CourseToFacultyModel);